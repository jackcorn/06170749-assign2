# Assignment 2 for 159.251 Backup Program 
## By Jack Cornford - ID:06170749
I ended up starting late and unsure as to how to get a reliable partner for the assignment so this was all done by me alone.

## Significant checkins:
* https://bitbucket.org/jackcorn/06170749-assign2/commits/6f1d9e4c9a62b60f552183c940234ff49808e288
* https://bitbucket.org/jackcorn/06170749-assign2/commits/ef58a334a6cbf7370f774ce5845a54111941e463

The core program is all contained in two files (which should in the same directory).

* mybackup.py is the core program which will accept the arguments as per the assignment spec.
* mybackupcore.py contains the core programming logic - separated off so that unit test can be easily implemented

* myBackupTests.py contains an array of unit tests used to check the functioning of the core application. If in the same directory then they can all be used together. 
The unit tests rely on a subdirectory containing sample files for testing purposes. It should be in a subdirectory relative to the mybackupTests.py executable which should be run from the directory it is installed in. 

I actually ended up using PTVS so have included in the respository all the project files needed also and the compiled pyc files which can be used instead of the .py files on a windows system.

Hope that is all ok.

Jack