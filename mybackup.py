#!/usr/bin/env python
import mybackupcore, argparse

#==========================================================================
# Primary Script Code and Main Module
#==========================================================================

parser = argparse.ArgumentParser()
parser.add_argument("command", nargs = "?", default="help")
parser.add_argument("target", nargs = "?", default="")

args = parser.parse_args()
command = args.command.lower()
target = args.target.lower()

if args.command == "help":
    mybackupcore.help()
elif command == "init":
    mybackupcore.init(target)
elif command == "store":
    mybackupcore.store(args.target)
elif command == "list":
    mybackupcore.list(target)
elif command == "test":
    mybackupcore.test(target)
elif command == "get":
    mybackupcore.get(target)
elif command == "restore":
    mybackupcore.restore(target)
else:
    print "Unrecognised argument:" + command
    mybackupcore.help()
