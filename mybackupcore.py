import pickle, sys, os, hashlib, shutil, time, logging, logging.handlers

myArchivePath = os.path.join(os.path.expanduser("~"),"Desktop", "myArchive")
myObjectsPath = os.path.join(myArchivePath,"objects")
myIndexPath = os.path.join(myArchivePath,"index.pkl")

logger = logging.getLogger("mybackup")
LOG_FILENAME      = 'mybackup.log'
CONSOLE_LOG_LEVEL = logging.ERROR  # Only show errors to the console
FILE_LOG_LEVEL    = logging.INFO   # but log info messages to the logfile
logger.setLevel(logging.DEBUG)
# FILE-BASED LOG
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh = logging.FileHandler(os.path.join(myArchivePath,LOG_FILENAME))
fh.setLevel(FILE_LOG_LEVEL)
fh.setFormatter(formatter)
logger.addHandler(fh)
logger.info('\n---------\nLog started on %s.\n---------\n' % time.asctime())
ch = logging.StreamHandler()
ch.setLevel(CONSOLE_LOG_LEVEL)
formatter = logging.Formatter('%(message)s')     # simpler display format
ch.setFormatter(formatter)
logger.addHandler(ch)
    
#=================================================================================================
# Help Text and Error Dscription Functions
#=================================================================================================
def help():
    print "MyBackup is designed to archive your files and folders."
    print "\r\n  Usage:"
    print "    mybackup init                         <-- initialise your archive"
    print "    mybackup store <directory>            <-- store the entire directory"
    print "                                              into your archive recursively"
    print "    mybackup list                         <-- list the contents of the archive"
    print "    mybackup list <pattern>               <-- list the contents of the archive"
    print "                                              that match the pattern"
    print "    mybackup test                         <-- check the integrity of the "
    print "                                              archive"
    print "    mybackup get <filename>               <-- retrieve a single file from the "
    print "                                              archive"
    print "    mybackup get <pattern>                <-- retrieve all files matching the "
    print "                                              pattern from the archive"
    print "    mybackup restore <destination>        <-- restore the entire archive to the"
    print "                                              named directory"

#==========================================================================
# Utility Functions
#==========================================================================
def getFileHash (filename):
    """CreateFileHash (file): create a signature for the specified file
       Returns SHA1 hash
    """

    f = None
    signature = None
    try:
        filesize  = os.path.getsize(filename)
        modTime   = int(os.path.getmtime(filename))

        f = open(filename, "rb")  # open for reading in binary mode
        hash = hashlib.sha1()
        s = f.read(16384)
        while (s):
            hash.update(s)
            s = f.read(16384)
        signature = hash.hexdigest()     
    except IOError:
        signature = None
    except OSError:
        signature = None
    finally:
        if f: 
            f.close()
    return(signature)

def archiveExists():
    return (os.path.exists(myArchivePath) and os.path.isdir(myArchivePath) and os.path.exists(myObjectsPath) and os.path.isdir(myObjectsPath) and os.path.exists(myIndexPath))

def validateArchive():
    if not archiveExists():
        print "No archive found. Please run 'myBackup init' to create a new archive"
        return False
    else:
        return True

def getIndex():
    return pickle.load(open(myIndexPath))

def saveIndex(index):
    pickle.dump( index, open( myIndexPath, "wb" ))

def filterIndex(index, filter):
    if filter == "":
        return index
    return {keys:values for (keys,values) in index.iteritems() if filter in keys}



#==========================================================================
# Primary Command Functions
#==========================================================================

def init(target):
    if (target == ""):
        if archiveExists():
            print "The archive already exists (no action necessary)"
        else:
            if not os.path.exists(myArchivePath):
                os.makedirs(myArchivePath)         
            if not os.path.exists(myObjectsPath):
                os.makedirs(myObjectsPath)   
            if not os.path.exists(myIndexPath):
                index = dict()
                saveIndex(index)
            print "Archive created successfully"    
    else:
        print "Unrecognised parameter:" +  target
        help()

def store(target):
    logger.info('Store called. Target:' + target)
    if not (os.path.exists(target) and os.path.isdir(target)):
        print "Directory to be stored not found: " + target
    elif validateArchive():
        index = getIndex()
        for root, dir, files in os.walk(target):
            for file in files:
                filepath = os.path.abspath(os.path.join(root,file))
                hash = getFileHash(filepath)
                index[filepath] = hash
                print filepath
                print hash
                shutil.copy(filepath, os.path.join(myObjectsPath, hash))
                logger.info('File Stored. FilePath:' + filepath)
        saveIndex(index)

def list(target):
    if validateArchive():
        for filepath, hash in filterIndex(getIndex(),target).iteritems():
            print filepath

def test(target):
    if validateArchive():
        countOk = 0
        for filepath, hash in filterIndex(getIndex(),target).iteritems():
            targetpath = os.path.join(myObjectsPath, hash)
            if (os.path.exists(targetpath) and os.path.isfile(targetpath)):
                if getFileHash(targetpath) == hash:
                    countOk += 1
                else:
                    print "File does not match hash: " + filepath
            else:
                print "File in Index not in archive: " + filepath
        print "Count of correct files in archive: " + str(countOk)

def conditionalCopy(source,target):
    if os.path.exists(target):
        userInput = raw_input("File already exists. Overwrite (y/n):")
        if userInput.lower <> "y":
            return
        logger.info('File overwritten by restore/get. Target:' + target)
    shutil.copy(source,target)

def get(target):
    if validateArchive():
        filteredIndex = filterIndex(getIndex(),target)
        if len(filteredIndex) == 0:
            print "No Files found"
        elif len(filteredIndex) == 1:
            for filepath, hash in filteredIndex.iteritems():
                conditionalCopy(os.path.join(myObjectsPath, hash), os.path.join(".", os.path.basename(filepath)))
        else:
            count = 0
            for filepath, hash in filteredIndex.iteritems():
                count +=1
                print str(count) + " " + filepath
                if count >= 50:
                    break
            userInput = raw_input("Multiple results found. Enter number of file to restore.")
            if userInput.isdigit():
                selection = int(userInput)
                if selection == 0 or selection > count:
                    print "Invalid selection. Need a number between 1 and " + str(count)
                    return
                item = filteredIndex.keys()[selection - 1]
                print item
                conditionalCopy(os.path.join(myObjectsPath, filteredIndex[item]), os.path.join(".", os.path.basename(item)))
            else:
                print "Invalid selection. Enter a number"

def restore(target):
    if validateArchive():
        if not os.path.exists(target):
            print "Restore path not found:" + target
            return
        if os.path.isfile(target):
            print "Restore path is not a valid directory:" + target
            return
        for filepath, hash in getIndex().iteritems():
            drive, path = os.path.splitdrive(os.path.dirname(filepath))
            targetDir = os.path.join(os.path.abspath(target), path[1:])
            if not os.path.exists(targetDir):
                os.makedirs(targetDir)
            shutil.copy(os.path.join(myObjectsPath,hash), os.path.join(targetDir,os.path.basename(filepath)))
                