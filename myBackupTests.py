import unittest
import mybackupcore
import shutil
import os
import StringIO
import sys

def deleteStuff(target):
    if os.path.exists(target):
        if os.path.isdir(target):
            shutil.rmtree(target, ignore_errors=True)
        else:
            os.remove(target)

class Test_myBackupTests(unittest.TestCase):
    def setUp(self):
        self.output = StringIO.StringIO()
        sys.stdout = self.output

    def testInit(self):
        deleteStuff(mybackupcore.myArchivePath)
        mybackupcore.init("")
        assert self.output.getvalue() == "Archive created successfully\n"
        assert mybackupcore.archiveExists()

    def testLoadAndTest(self):
        deleteStuff(mybackupcore.myArchivePath)
        mybackupcore.init("")
        mybackupcore.store("testfiles")
        os.remove(os.path.join(mybackupcore.myObjectsPath,"7d28560740ae14c7ffb5ee3c272f7068277cf2ff"))
        shutil.copy(os.path.join("testfiles", "chicken.txt"), os.path.join(mybackupcore.myObjectsPath,"bcfde7b8e0b055ad072d469005e4571c73890e22"))
        mybackupcore.test("")
        testOutput = self.output.getvalue()
        assert testOutput.find("Count of correct files in archive: 4\n") > 0
        assert testOutput.find("File in Index not in archive: " + os.path.abspath(os.path.join("testfiles","How to get rid of Telemarketers.txt"))) > 0
        assert testOutput.find("File does not match hash: " + os.path.abspath(os.path.join("testfiles", "GRANDMA.txt"))) > 0

    def testLoadAndGet(self):
        deleteStuff("chicken.txt")
        deleteStuff(mybackupcore.myArchivePath)
        mybackupcore.init("")
        mybackupcore.store("testfiles")
        mybackupcore.get("chicken.txt")
        assert os.path.exists("chicken.txt")
        deleteStuff("chicken.txt")

    def testLoadAndRestore(self):
        restoreTestPath = os.path.join(os.path.expanduser("~"),"Desktop", "myRestoreTest")
        deleteStuff(restoreTestPath)
        os.makedirs(restoreTestPath)
        mybackupcore.init("")
        mybackupcore.store("testfiles")
        mybackupcore.restore(restoreTestPath)
        targetdrive,targetpath = os.path.splitdrive(os.path.abspath("testfiles"))
        fulltargetPath = os.path.join(restoreTestPath, targetpath[1:])
        assert os.path.exists(os.path.join(fulltargetPath,"chicken.txt"))
        assert os.path.exists(os.path.join(fulltargetPath,"GRANDMA.txt"))
        deleteStuff(restoreTestPath)
        
if __name__ == '__main__':
    unittest.main()
